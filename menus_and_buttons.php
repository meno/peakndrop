
<?php

// menus ( for right clic)
$PKDP_menus=" 
    <dialog id=\"menufile\" class=\"menu dialog\">
        <div class=\"action\" onclick=\"showFileContent();\">Affiche contenu</div>
        <div class=\"action\" onclick=\"cepStatus();\">Descendants</div>
        <div class=\"action\" onclick=\"action('loulou');\">loulou</div>
    </dialog>
    <dialog id=\"menuline\" class=\"menu dialog\">
        <div class=\"action\" onclick=\"alert('coucou');\">coucou</div>
        <div class=\"action\" onclick=\"alert('bibi');\">bibi</div>
        <div class=\"action\" onclick=\"action('loulou');\">loulou</div>
    </dialog>
    <dialog id=\"menufolder\" class=\"menu dialog\">
        <div class=\"action\" onclick=\"alert('folder');\">Folder</div>
        <div class=\"action\" onclick=\"cepStatus();\">Descendants</div>
        <div class=\"action\" onclick=\"action('loulou');\">loulou</div>
    </dialog>";


// configuration des boutons pour action sur un arbre ou un répertoire
$PKDP_tree_buttons = [
  'caret-left' => [_('Replace with the previous content'),'PreviousContent'], 
  'caret-right' => [_('Replace with the next content'),'NextContent'],
  'empty' => [_('Empty this file'),'empty'],
 // 'row-insert-top' => [_('Add lines at the Beginning of this file'),'addAtBeginning'],
  'edit' => [_('Edit this file'),'editFile'],
  'fold' => [_('Fold up'),'fold'],
  'unfold' => [_('Unfold all'),'unfold'],
];
$PKDP_folder_buttons = [
    'caret-left' => [_('Replace with the previous content'),'PreviousContent'],
    'caret-right' => [_('Replace with the next content'),'NextContent'],
    'folder' => [_('Add a new folder'),'newFolder'],
    'file' => [_('Add a new file'),'newFile'],
    'fold' => [_('Fold up'),'fold'],
    'unfold' => ['Unfold all','unfold'],
];

$PKDP_principal_menu = [
   'home' => [_('Open home folder in the B zone'),"folder_in_container('files/','B')"],
   'brightness' => [_('Dark/Light'),'darkLight'],
   'settings' => [_('Settings'),'settings'],
   'search' => [_('Search'),'recherche'],
];
$PKDP_actions2 = [
   'beamer' => [_('Export in mode presentation (Beamer/LaTeX)'),'presentationp'],
   'poly' => [_('Export in mode corpus 2 col (LaTeX)'),'poly'],
   'poly1c' => [_('Export in mode poly 1 col (LaTeX)'),'poly1c'],
   'polyp' => [_('Export paysage 2 col'),'poly'],
   'markdown' => [_('Markdown'),'markdown'],
   'presentationp' => [_('Presentation'),'presentation'],
   'brut' => [_('Brut'),'brut'],
   'html5' => [_('Html5'),'html5'],
   'markdown' => [_('Mardown'),'markdown'],
   'outline' => [_('Outline'),'outline'],

]

?>
