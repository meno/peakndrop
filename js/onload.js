/** javascript function to add all other functions on the occurrence of "onload" event. Just a call to addtoOnLoad() with parameter as function name will automatically queue up your functions to be called on onload. 

Thanks to https://gist.github.com/lihost/614587

**/

function addtoOnLoad(func) {
    var preOnload = window.onload;
    if(typeof window.onload != 'function'){
        window.onload = func;
    } else {
        window.onload = function(){
            if(preOnload){
                preOnload();
            }
            func();
        }
    }
}
