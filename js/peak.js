let JS_debug = false;

const CLASS_NAMES = {
  HIDDEN: "invisible",
  DROPZONES: "dropzone",
  INTER: "inter",
  HIDDING_BUTTON: "hidding_button",
  LABEL_HIDDING_BUTTON: "label_for_hidding_button",
  CEP: "tree",
  HIDE_TREE_LINK: "invisible_tree_link",
};

const ATTRIBUT_NAMES = {
  DROP_ACTIF: "drop-active",
  SORTABLE: "sortable",
};

const HIDDABLE = true;
const NOT_HIDDABLE = false;
const ID_BUTTON = "id_button";

let dragItem = null;
let oldParent = null;
let oldLine = null;
let onlyoneStart = false;
let onlyoneDrop = false;
let dragCount = 0;
let idButtonCounter = 0;

/**
 * grants the given node to be drag'n drop into any droppable node
 * @param {Node} foo the draggable node
 * @author Felie & Kedesiklem
 */

function makeDraggable(foo, sortable) {
  foo.addEventListener("dragstart", dragStart);
  foo.addEventListener("dragend", dragEnd);
  foo.setAttribute("draggable", true);
  if (sortable) {
    foo.setAttribute(ATTRIBUT_NAMES.SORTABLE, true);
    foo.insertAdjacentHTML("afterbegin", makeIntercalaire());
    Array.from(foo.getElementsByClassName(CLASS_NAMES.INTER)).forEach((inter) =>
      makeDroppable(inter, false)
    );
  }
}

/**
 * allow any draggable node to be drop inside the given node
 * @param {Node} foo the droppable node
 * @param {boolean} hiddable give the node the possibility to hide its children with a checkbox input
 * @author Felie & Kedesiklem
 */
function makeDroppable(foo, hiddable) {
  foo.classList.add(CLASS_NAMES.DROPZONES);
  foo.addEventListener("dragover", dragOver);
  foo.addEventListener("dragleave", dragLeave);
  foo.addEventListener("drop", dragDrop);
  if (hiddable) {
    addHiddingButton(foo);
    updateHiddingButton(foo);
  }
}

function makeIntercalaire() {
  return "<div class='" + CLASS_NAMES.INTER + "'></div>";
}

function dragLeave(e) {
  //console.log("dragleave event");
  //console.log(e.target);
  if (isOnDisk(dragItem)) {
    // for the case of file => tree
    getCep(e.target).removeAttribute(ATTRIBUT_NAMES.DROP_ACTIF);
  }
  getClosestDropzone(e.target).removeAttribute(ATTRIBUT_NAMES.DROP_ACTIF);
}

function dragOver(e) {
  g = getClosestDropzone(e.target);
  if (
    (isOnDisk(g) && (isFOLDER(g) || isRoot(g)) && !isOnDisk(dragItem)) ||
    (isOnDisk(dragItem) && isOnDisk(g) && !(isFOLDER(g) || isRoot(g)))
    //|| (isOnDisk(dragItem) && !isOnDisk(g))
  )
    return;
  if (isOnDisk(dragItem) && !isOnDisk(g))
    // case of file on tree
    getCep(e.target).setAttribute(ATTRIBUT_NAMES.DROP_ACTIF, true);
  else
    getClosestDropzone(e.target).setAttribute(ATTRIBUT_NAMES.DROP_ACTIF, true);
  e.preventDefault();
}

function dragStart() {
  dragCount++;
  if (onlyoneStart) return false;
  onlyoneStart = true;
  dragItem = this;
  oldLine = getLine(dragItem);
  oldParent = this.parentNode.parentNode;
  this.classList.add(CLASS_NAMES.HIDE_TREE_LINK);
  setTimeout(() => this.classList.add(CLASS_NAMES.HIDDEN), 0);
}

function dragEnd() {
  this.classList.remove(CLASS_NAMES.HIDE_TREE_LINK);
  this.classList.remove(CLASS_NAMES.HIDDEN);
  dragItem = null;
  oldLine = null;
  oldParent = null;
  onlyoneStart = false;
  onlyoneDrop = false;

  // Retire la surbrillance de toutes les zones de dépôt
  document.querySelectorAll(`.${CLASS_NAMES.DROPZONES}`).forEach((zone) => {
    zone.removeAttribute(ATTRIBUT_NAMES.DROP_ACTIF);
  });
}

function failDrop() {
  couine();
  dropTarget.removeAttribute(ATTRIBUT_NAMES.DROP_ACTIF);
}

function opened(cep) {
  var trees = document.querySelectorAll(".root.tree");
  //console.log(trees);
  result = false;
  trees.forEach((tree) => {
    if (cep == tree.id) result = true;
  });
  return result;
}

let MajPressed = false;
function KeyPressCheck(event) {
  if (event.which == 16) {
    MajPressed = true;
    //console.log("MAJ pressed");
  }
}

// Toutes les interactions qui n'ont pas de sens
function isInvalidDragDrop(dragItem, dropTarget) {
  return (
    ((isFILE(dragItem) || isLINE(dragItem)) && isFILE(dropTarget)) || // fichier ou ligne dans un fichier : à retirer pour integrer l'ajout un append (avec confirmation de l'utilisateur)
    (isLINE(dragItem) && isFOLDER(dropTarget)) || // ligne dans dossier : pas de sens
    (isFILE(dragItem) && isLINE(dropTarget)) || // fichier dans ligne : pas de sens
    (isFOLDER(dragItem) && (isFILE(dropTarget) || isLINE(dropTarget))) || // dossier dans fichier ou dans ligne : pas de sens
    (isFOLDER(dragItem) && !isOnDisk(dropTarget)) // les dossier doivent rester dans les zone onDisk
  );
}

function getCookieValue(name) // thanks to https://stackoverflow.com/questions/10730362/get-cookie-by-name
    {
      const regex = new RegExp(`(^| )${name}=([^;]+)`)
      const match = document.cookie.match(regex)
      if (match) {
        return match[2]
      }
   }

function couine(){
  if (getCookieValue('sound') == 'on')
    new Audio("sounds/fail.mp3").play();
}

function clic(){
  if (getCookieValue('sound') == 'on')
    new Audio("sounds/clic.mp3").play();
}

function dragDrop(e) {
  // Affiche l'élément sur lequel on effectue le drop et identifie la zone de dépôt.
  dropTarget = getClosestDropzone(e.target);

  // Vérifie s'il y a des éléments à déplacer. Si non, on sort de la fonction.
  if (dragCount == 0) return false;
  dragCount--; // Décrémente le compteur d'éléments en cours de drag.

  // Si l'élément ne fait pas partie d'une zone de dépôt, on ignore l'opération.
  if (!this.classList.contains(CLASS_NAMES.DROPZONES)) return true;

  // Conditions de validation pour le dépôt.
  if (isInvalidDragDrop(dragItem, dropTarget)) return failDrop();

  dropTarget.removeAttribute(ATTRIBUT_NAMES.DROP_ACTIF); // Marque la zone de dépôt comme inactive.

  // Si on déplace un élément depuis le disque vers une nouvelle destination.
  if (isOnDisk(dragItem) && !isOnDisk(dropTarget)) {
    source = getPath(dragItem); // Chemin de l'élément déplacé.
    destination = getTitle(getCep(dropTarget)); // Récupère le titre de la cible.
    container = getCep(dropTarget).getAttribute('container');
    //alert('container='+container+'destination='+destination+' source='+source);
    replace_cep(source,destination,container); // Remplace le chemin source par la destination.
    cepSetStatus(source); // Met à jour l'état de l'élément source.
    events_for_buttons(); // Réinitialise les événements des boutons.
    clic(); // Joue un son de confirmation.
    return; // Fin de la fonction après le traitement du déplacement.
  }

  // Cas où l'on déplace un élément non sur disque vers un élément sur disque.
  if (
    !isOnDisk(dragItem) &&
    isOnDisk(dropTarget)
    // !isFOLDER(dropTarget) &&
    // !isRoot(dropTarget)
  ) {
    oldFile = getCep(oldParent).id; // ID de l'ancien parent.
    cep = getCep(dragItem); // Récupère l'élément à déplacer.
    newFile = dropTarget.classList.contains("button")
      ? dropTarget.getAttribute("url")
      : getPath(dropTarget);
    TargetDiv = getCep(dropTarget).id; // ID de la cible.

    // Prépare les données pour l'appel AJAX afin de déplacer l'élément dans un fichier.
    dataAjax =
      "maj=" +
      MajPressed +
      "&newFile=" +
      newFile +
      "&oldLine=" +
      oldLine +
      "&oldFile=" +
      oldFile +
      "&lineFather=" +
      getLine(this) +
      "&TargetDiv=" +
      TargetDiv +
      "&button=" +
      dropTarget.classList.contains("button");

    dragItem.remove(); // Supprime l'élément du DOM.
    updateHiddingButton(oldParent); // Met à jour l'état des boutons pour l'ancien parent.
    cepStoreStatus(cep); // Enregistre l'état de l'élément déplacé.
    ajax("ajax.php?action=moveIntoFile", dataAjax, "info"); // Appel AJAX pour déplacer l'élément.
    clic(); // Joue un son de confirmation.
    return; // Fin de la fonction.
  }

  // Si aucune des conditions n'est remplie, on joue un son et on sort de la fonction.
  clic();
  if (onlyoneDrop) return false; // Si un seul dépôt est autorisé, on ignore les autres.

  // Validation de la zone de dépôt pour empêcher des mouvements invalides.
  if (
    oldParent == dropTarget &&
    !dragItem.getAttribute(ATTRIBUT_NAMES.SORTABLE)
  )
    return false;

  onlyoneDrop = true; // Marque qu'un seul dépôt a été effectué.

  // Gestion des intercalaires.
  if (this.classList.contains(CLASS_NAMES.INTER)) {
    // Insère l'élément avant l'élément parent si c'est un intercalaire.
    li = this.parentNode;
    li.parentNode.insertBefore(dragItem, li);
  } else {
    // Ajoute l'élément à une liste (ul) ou crée une nouvelle liste si nécessaire.
    ul = this.getElementsByTagName("ul")[0];
    if (ul) {
      ul.appendChild(dragItem);
    } else {
      ul = document.createElement("ul");
      this.appendChild(ul);
      ul.append(dragItem);
    }
  }

  // Met à jour les boutons cachés pour les anciens et nouveaux parents.
  if (this != oldParent) {
    updateHiddingButton(this);
    updateHiddingButton(oldParent);
  }

  // Prépare les données AJAX pour le déplacement selon le type d'élément.
  if (isOnDisk(dragItem)) {
    dataAjax =
      "file=" +
      getTitle(dragItem) +
      "&from=" +
      getPath(oldParent) +
      "&to=" +
      getPath(dropTarget);
    action = "moveFile"; // Définit l'action à "moveFile".
  } else if (isLINE(dragItem)) {
    // Si l'élément déplacé est une liste.
    oldFile = getCep(oldParent).id; // Récupère l'ID de l'ancien parent.
    newFile = getCep(dragItem).id; // Récupère l'ID de l'élément déplacé.
    dataAjax =
      "oldLine=" +
      oldLine +
      "&oldFile=" +
      oldFile +
      "&newLine=" +
      getLine(dragItem) +
      "&lineFather=" +
      getLine(this) +
      "&newFile=" +
      newFile +
      "&into=" +
      Number(!this.classList.contains(CLASS_NAMES.INTER)); // Indique si on déplace dans une zone d'intercalaires.

    action = "moveTree"; // Définit l'action à "moveTree".
  } else return; // Quitte la fonction si aucune condition n'est remplie.

  update_IRL(dataAjax, action); // Met à jour l'état en temps réel via AJAX.
}

function isOnDisk(ele) {
  return ele.classList.contains("ondisk");
}

function isFOLDER(ele) {
  // FOLDER
  return ele.classList.contains("folder");
}

function isRoot(ele) {
  // CONTAINER ZONE
  return ele.classList.contains("root");
}

function isFILE(ele) {
  // FILE AS TREE
  return ele.classList.contains("file");
}

function isLINE(ele) {
  return ele.classList.contains("line") || ele.classList.contains("list");
}

function getTitle(item) {
  //console.log(item.getElementsByClassName("title"));
  if (item.classList.contains(CLASS_NAMES.CEP)) return item.id;
  if (item.getElementsByClassName("title")[0] != undefined)
    return item.getElementsByClassName("title")[0].innerHTML;
  else return undefined;
}

function getPath(item) {
  if (item.classList.contains(CLASS_NAMES.CEP)) return getTitle(item);
  if (item.classList.contains(CLASS_NAMES.INTER))
    return "juste avant " + getPath(item.parentNode);
  return getPath(getParent(item)) + "/" + getTitle(item);
}

function getLine(ele) {
  return Array.from(getCep(ele).getElementsByTagName("LI")).indexOf(ele);
}

function getCep(item) {
  if (item.classList.contains(CLASS_NAMES.CEP)) return item;
  return getCep(getParent(item));
}

function getParent(element) {
  if (element.classList.contains(CLASS_NAMES.INTER)) return element.parentNode;
  return element.parentNode.parentNode;
}

function getClosestDropzone(ele) {
  if (ele.classList.contains(CLASS_NAMES.DROPZONES)) return ele;
  return getClosestDropzone(ele.parentNode);
}

function update_IRL(data, action) {
  ajax("ajax.php?action=" + action, data, "info");
}

function basename(path) {
  return path.split("/").reverse()[0];
}

// --------------------------- HIDDING BUTTON HANDLER ----------------------------------

function addHiddingButton(foo) {
  foo.setAttribute(CLASS_NAMES.HIDDING_BUTTON, true);
  idButtonCounter++;
  let id = ID_BUTTON + "_" + idButtonCounter;
  let b =
    "<input style='display:none' class='" +
    CLASS_NAMES.HIDDING_BUTTON +
    "' id='" +
    id +
    "' type='checkbox'><label class='" +
    CLASS_NAMES.LABEL_HIDDING_BUTTON +
    "' for='" +
    id +
    "'></label>";
  foo.insertAdjacentHTML("afterbegin", b);
}

function updateHiddingButton(t) {
  if (t.getAttribute(CLASS_NAMES.HIDDING_BUTTON)) {
    li = t.getElementsByTagName("li");
    let lhb = t.getElementsByClassName(CLASS_NAMES.LABEL_HIDDING_BUTTON)[0]
      .classList;
    if (li.length != 0) {
      lhb.remove(CLASS_NAMES.HIDDEN);
    } else {
      lhb.add(CLASS_NAMES.HIDDEN);
    }
  }
}

// --------------------------- Ajax zone -----------------------------------------------

function ajax(target, dataAjax, div) {
  var xhr = new XMLHttpRequest();
  xhr.open("POST", target, false); // false for synchroneous ! very lightweight data...
  xhr.setRequestHeader(
    "Content-Type",
    " application/x-www-form-urlencoded; charset=UTF-8"
  );
  xhr.onreadystatechange = () => {
    if (xhr.readyState == XMLHttpRequest.DONE) {
      if (xhr.status == 200) {
        //alert('contenu='+xhr.responseText);
        document.getElementById(div).innerHTML = xhr.responseText;
      } else if (xhr.status == 400) {
        if (JS_debug) alert("There was an error 400");
      } else {
        if (JS_debug)
          alert(xhr.status + " something else other than 200 was returned");
      }
    }
  };
  xhr.onload = function () {
    if (xhr.status == 201) {
      console.log("Post successfully created!");
    }
  };
  xhr.send(encodeURI(dataAjax));
}

//------------ gestion des statuts ----------

function cepStoreStatus(cep) {
  // update status of a cep on disk
  var status = "";
  ondisk = cep.classList.contains("ondisk");
  cep = cep.id;
  descendants = Array.from(
    document.querySelectorAll("#" + cep + " ." + CLASS_NAMES.HIDDING_BUTTON)
  );
  descendants.forEach((d) => {
    status = status + Number(d.checked);
  });
  // gestion des status
  //ajax('ajax.php?action=status','cep='+cep+'&status='+status,'status-'+cep); // to store status on disk
}

function cepSetStatus(cep) {
  //console.log('cep='+cep);
  var status = document.getElementById("status-" + cep);
  //console.log(status);
  if (status == undefined || status == "")
    // for the solo button-file not registrered || notthing to do
    return;
  status = status.innerHTML;
  //console.log("status=" + status);
  descendants = Array.from(
    document.querySelectorAll("#" + cep + " ." + CLASS_NAMES.HIDDING_BUTTON)
  );
  //console.log(descendants);
  i = 0;
  descendants.forEach((d) => {
    if (status.charAt(i) == "1") d.checked = true;
    i = i + 1;
  });
}

function action_container(action, container) {
  document.getElementById(container).innerHTML = "";
  ajax("ajax.php?action=" + action, dataAjax, container);
  prepare_cep(container); // events to move
  distribute_menus(); // events for right-click and left-click
}

function load_in_container(url,container,for_stack=1){
  ajax(
    "ajax.php?action=loadUrl",
    "container=" + container + "&source=" + url,
    "container-" + container
  );
  prepare_cep('container-' + container);
  distribute_menus(); // events for right-click and left-click
  if (for_stack == 1) // no action if order is given by forward or backward
    new_in_stack(url,container);
}

function replace_cep(source,destination,container) {
  /*var cep = basename(source); // on s'en fout pour 'instant
    if (opened(cep)) {
      alert(cep+_(' est déjà ouvert ailleurs '));
      return;
    }*/
  //container = document.getElementById(destination).parentNode.id;
  ajax(
    "ajax.php?action=loadTree",
    "container=" +
      container +
      "&source=" +
      source +
      "&destination=" +
      destination,
    'container-'+container
  ); // content
  //x = document.getElementById(cep);
  prepare_cep('container-'+container);
  //alert('coucou');
  distribute_menus(); // events for right-click and left-click
  //alert('in replace cep '+container)
  new_in_stack(source,container);
}

// ------------- vide un fichier dans un container et le réaffiche

function empty(path, ondisk) {
  // vide un fichier et le recharge vide
  if (ondisk || !confirm(sprintf(_("Vider vraiment %s ?"), path))) return;
  var cep = document.getElementById(path.replace(/\//g, "ø")).parentNode.id;
  alert(cep);
  ajax(
    "ajax.php?action=empty",
    "cep=" + cep + "&path=" + path + "&ondisk=" + ondisk,
    cep
  );
  prepare_cep(cep);
}

// --------------------------- forward/backward management

function NextContent(url,z){
  console.log(stack[z]);
  forward(z);
}

function PreviousContent(url,z){
  console.log(stack[z]);
  backward(z);
}

function init_stack(z,url){
  if (!window.stack){
    stack = {};
    index = [];
  }
  stack[z] = [url];
  index[z] = 0;
  console.log('after init  ='+stack[z]+' '+index[z]);
}

function new_in_stack(url,z){
  index[z] = index[z] + 1;
  stack[z].splice(index[z],stack[z].length - index[z]); // remove after actual index
  stack[z][index[z]] =  url;
  console.log('init '+stack[z]+' '+index[z]);
}

function forward(z){
  console.log('before forward ='+stack[z]+' '+index[z]);
  if (index[z] == stack[z].length-1)
    couine();
  else
    {
    index[z] = index[z] + 1;
    load_in_container(stack[z][index[z]],z,0);
    }
    console.log('after forward  ='+stack[z]+' '+index[z]);
}

function backward(z){
  console.log('before backward ='+stack[z]+' '+index[z]);
  if (index[z] == 0)
    couine();
  else 
    {
    index[z] = index[z] - 1;
    load_in_container(stack[z][index[z]],z,0);
    }
  console.log('after backward  ='+stack[z]+' '+index[z]);
}

// --------------------------- mobile detection

function isMobile() {
  return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
    navigator.userAgent
  );
}

// --------------------------- Initialisation ------------------------------------------

peakOnLoad = function () {
  const lists = Array.from(document.querySelectorAll(".list"));
  lists.forEach((i) => {
    makeDraggable(i, true);
    makeDroppable(i, HIDDABLE);
  });
  const folders = Array.from(document.querySelectorAll(".folder"));
  folders.forEach((i) => {
    makeDraggable(i, false);
    makeDroppable(i, HIDDABLE);
  });
  const files = document.querySelectorAll(".file");
  files.forEach((file) => {
    makeDraggable(file, false);
    makeDroppable(file, HIDDABLE); // <--- ajouté, pour qu'on puisse droper sur un fichier un contenu
  });
  const trees = document.querySelectorAll(".tree");
  trees.forEach((tree) => {
    makeDroppable(tree, NOT_HIDDABLE);
  });
};

function priorities() {
  liste = document.getElementsByClassName("title");
  for (var i = 0; i < liste.length; i++) {
    t = liste[i];
    if (t.innerHTML.substring(0, 3) == "!!!") t.classList.add("color3");
    else if (t.innerHTML.substring(0, 2) == "!!") t.classList.add("color2");
    else if (t.innerHTML.substring(0, 1) == "!") t.classList.add("color1");
    else t.classList.add("color0");
  }
}

function prepare_cep(cep) {
  var name = cep;
  cep = "#" + cep;
  //alert(cep);
  //console.log("prepare " + cep);
  const lists = Array.from(
    document.querySelectorAll(cep + ".list," + cep + " .list")
  );
  lists.forEach((i) => {
    makeDraggable(i, true);
    makeDroppable(i, HIDDABLE);
  });
  const folders = Array.from(
    document.querySelectorAll(cep + ".folder," + cep + " .folder")
  );
  folders.forEach((i) => {
    makeDraggable(i, false);
    makeDroppable(i, HIDDABLE);
  });
  const files = document.querySelectorAll(cep + ".file," + cep + " .file");
  files.forEach((file) => {
    makeDraggable(file, false);
    makeDroppable(file, HIDDABLE); // <--- ajouté, pour qu'on puisse droper sur un fichier un contenu
  });
  const trees = document.querySelectorAll(cep + ".tree," + cep + "  .tree");
  trees.forEach((tree) => {
    makeDroppable(tree, NOT_HIDDABLE);
  });
  cepSetStatus(name);
  priorities();
}

function events_for_buttons() {
  Array.from(document.getElementsByClassName(CLASS_NAMES.HIDDING_BUTTON)).forEach((b) => {
    //console.log("BUTTON : ", b);
    b.addEventListener("change", () => {
      // for PC
      cepStoreStatus(getCep(getClosestDropzone(b))); // store the status when cep display change
    });
  });

  Array.from(document.getElementsByClassName(CLASS_NAMES.LABEL_HIDDING_BUTTON)).forEach((b) =>
    b.addEventListener("click", () => {
      // for mobile
      target = document.getElementById(b.getAttribute("for"));
      //console.log(target);
      if (!isMobile()) return;
      target.checked = !target.checked;
      cepStoreStatus(getCep(getClosestDropzone(b))); // store the status when cep display change
    })
  );
}


function killcookie(name){ // kill a cookie by his name for the whole program (not path indication)
  document.cookie = name+'=; expires=Thu, 01 Jan 1970 00:00:00 UTC;';
}

peakOnLoad = function () {
  //console.log(allStatus);
  killcookie('init');
  const ceps = Array.from(document.getElementsByClassName("root")); // all the ceps - columns
  a = ""; // ???
  ceps.forEach((cep) => { 
    //alert(cep.getAttribute('container'));
    if (getCookieValue('init') == null){
        // if stack on disque, load stack and index
        init_stack(cep.getAttribute('container'),cep.id); // init stack 
    }
    prepare_cep(cep.id);
  });
  events_for_buttons();
  document.cookie = "init=1";
};

addtoOnLoad(peakOnLoad);
