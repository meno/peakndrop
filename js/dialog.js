var cursor_x = -1;
var cursor_y = -1;
document.onmousemove = function(event)
{
    cursor_x = event.pageX;
    cursor_y = event.pageY;
    
}

/*document.addEventListener('keypress', tFunction);
function tFunction (e){
    if(e.key === "Escape") {
        alert('coucou');
    }
}*/

function sendInput(){
    data = document.getElementById('input').value;
    container=document.getElementById(docActual.replace(/\//g,'ø')).parentNode.id;
    dataAjax='data='+data+'&container='+container+'&doc='+docActual+'&replaceContent='+replaceContent;
    dialog.close();
    document.getElementById('input').innerHTML = ''; // clear the textarea input in the DOM
    action_container('add',container);
}

const sprintf = ( format, ...args ) => ( i = 0, format.replace( /%s/g, () => args[i++] ) ); // thanks to r4phf43l at https://gist.github.com/rmariuzzo/8761698

function action(a)  {
    alert(a+' '+elementActual.innerHTML);
}

document.onkeydown = function(evt) { // fermeture dialogues par ESC 
    evt = evt || window.event;
    if (evt.keyCode === 27) {
      close_all_dialogs();
    }
};

function dialog_position(w){
    //dialog.style.marginLeft = (cursor_x+10)+'px';
    w.style.marginLeft = '50px';
    //dialog.style.marginTop = cursor_y+'px';
    w.style.marginTop = '50px';
}

function showFileContent() { // deux fonctions ??? qu'est-ce que c'est que ces boutons ?
    //alert('showFileContent');
    f=getClosestDropzone(elementActual); 
    is_button = f.classList.contains('button');
    if (!is_button)
        f=getPath(f);
    else
        f=f.getAttribute('url');
    //console.log(f)
    close_all_dialogs();
    view = document.getElementById('view');
    dialog_position(dialog);
    ajax('ajax.php?action=readContent','file='+f+'&ondisk=1&mask=1&button='+is_button,'view');
    //menuL.close();
    view.show();
}

function openDialog(){
    dialog = document.getElementById('dialog');  
    dialog_position(dialog);
    close_all_dialogs(); 
    dialog.show();
}

function addAtBeginning(path) {
    docActual = basename(path);
    replaceContent = false;
    openDialog();
}

function editFile(path,ondisk) { 
    dataAjax = 'file='+path+'&ondisk='+ondisk+'&mask=no';
    //console.log(dataAjax);
    ajax("ajax.php?action=readContent",dataAjax,'textarea');
    input = document.getElementById('input');
    docActual = path; // variable for SendInput
    typeActual = ondisk;
    replaceContent = true;
    openDialog();
}

function distribute_menu(){ // right button
    elements=Array.from(document.getElementsByClassName('wmenu'));
    elements.forEach(element => { element.oncontextmenu = function() { 
        menu=document.getElementById('menu'+element.getAttribute('menu'));
        event.preventDefault();
        menu.style.marginLeft = (cursor_x+10)+'px';
        menu.style.marginTop = cursor_y+'px';
        elementActual = element;
        close_all_dialogs();
        menu.show();
    };}); 
}

function simpleClick(){
    dialog_position(view);
    showFileContent(); 
}

function ctrlClick(){
    source = getPath(elementActual); // Chemin de l'élément déplacé.
    //console.log('source='+source);
    destination = getTitle(getCep(elementActual));
    container=getCep(elementActual).getAttribute('container');
    //console.log('destination='+destination);
    replace_cep(source,destination,container); // Remplace le chemin source par la destination. 
    cepSetStatus(source)

}

function distribute_view(){
    view = document.getElementById('view'); // left button   
    elements=Array.from(document.querySelectorAll('.file'));
    elements.forEach(element => { element.onclick = function(event) {
        elementActual = element;
        if (event.ctrlKey){
            event.preventDefault();
            ctrlClick()
        }
        else
            simpleClick();

    }});
}

function close_all_dialogs(){
  elements=Array.from(document.getElementsByClassName('dialog'));
  elements.forEach(element => element.close());
  view.close();
  }

function distribute_menus(){
  distribute_menu();
  distribute_view();
}

dialogToload = function () {
    distribute_menus();
}

addtoOnLoad(dialogToload);

