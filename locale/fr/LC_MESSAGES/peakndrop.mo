��    
      l      �       �      �                       0     8  !   V  
   x     �  J  �     �     �          +     >      K  $   l     �  !   �        
                        	             Add a new file Add a new folder Edit this file Empty this file Fold up Replace with the next content Replace with the previous content Unfold all Vider vraiment %s ? Project-Id-Version: logintool 0.1
PO-Revision-Date: 2024-12-01 18:30+0100
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SearchPath-0: .
 Ajouter un nouveau fichier Ajouter un nouveau dossier Éditer ce fichier Effacer ce fichier Replier tout Remplacer par le contenu suivant Remplacer par le contenu précédent Déplier tout Vous voulez vraiment effacer %s ? 