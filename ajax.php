<?php
require_once('config.php');
require_once('util.php');
if (!isset($_SESSION))
  session_start();
//echo session_id();
/*print_r($_GET);
print_r($_POST);*/
function correction($s, $oldi, $newi)
{ // corrige l'indentation
    if ($newi > $oldi) { // add
        $bout = str_repeat(' ', $newi - $oldi);
        foreach ($s as &$l) {
            $l = $bout . $l;
        }
    } else if ($newi <= $oldi) { // remove
        $lbout = $oldi - $newi;
        foreach ($s as &$l) {
            $l = substr($l, $lbout, strlen($l) - $lbout);
        }
    }
    return $s;
}

function treeToMove($oldLine,$indent,$source) { // get the tree to move 
    $i = $oldLine;
    $toMove = [];
    do {
        $toMove[] = $source[$i];
        $i++;
    } while ($i < sizeof($source) and indentOf($source[$i]) > $indent);
    return $toMove;
}

function Pather($x){
    global $_POST;
    return str_replace('ø','/',$_POST[$x]);
}

//view($_SESSION);

switch ($_GET['action']){
    case 'loadUrl':
        if (is_dir("$DATA/".Pather('source')))
            $_GET['action'] = 'loadFolder';
        else
            $_GET['action'] = 'loadTree';
}

// calculation of return of the ajax call
switch ($_GET['action']) { 
    case 'moveFile': // move of a file or directory in a filesystem
        $file = $_POST['file'];
        rename("$DATA/".Pather('from')."/$file", "$DATA/".Pather('to')."/$file");
        break;
    case 'moveIntoFile':
        $_POST['newLine'] = 0; // at the beginning of the target file
        $newFile = Pather('newFile');
        $_POST['into'] = 0; // irrelevant but necessary
    case 'moveTree': // move of a branch in a tree stored in a file
        $into = $_POST['into'];
        $lineFather = $_POST['lineFather'];
        //$oldFile = PKDP_path($_POST['oldFile'],'',0);  ??? inutile
        $oldFile = "$DATA/".Pather('oldFile');
        $oldLine = $_POST['oldLine'];
        if (!isset($newFile))
            //$newFile = PKDP_path($_POST['newFile'],'',0); //"$DATA/".$_SESSION['elements'][$_POST['newFile']];
            $newFile = "$DATA/".Pather('newFile');
        //echo "newFile=$newFile<br>oldFile=$oldFile<br>";
        $newLine = $_POST['newLine'];
        $source = string2array(getOrCreateFile($oldFile));
        $indent = indentOf($source[$oldLine]);
        $toMove = treeToMove($oldLine,$indent,$source); // calculation of to move
        $lgtomove = sizeof($toMove);
        // remove $toMove from source
        $source = array_merge(array_slice($source, 0, $oldLine), array_slice($source, $oldLine + $lgtomove, sizeof($source) - $oldLine - $lgtomove));
        file_put_contents($oldFile, array2string($source));     // store new $source
        $destination = string2array(getOrCreateFile($newFile)); // load $destination (if destination = source ok)
        $sizeDestination = sizeof($destination);
        // destination indent
        if ($newLine==0 or $newLine>$sizeDestination or ($into==1 and $lineFather==-1))
            $destIndent=0;
        else
            if ($into==1)
                    $destIndent = indentOf($destination[$lineFather])+1;
            else
                $destIndent = indentOf($destination[$newLine]);
        // correction of toMove indent
        $toMove = correction($toMove, $indent, $destIndent);
        // insert toMove à la destination
        $destination = file_put_contents($newFile,array2string(array_merge(array_slice($destination, 0, $newLine), $toMove, array_slice($destination, $newLine, sizeof($destination) - $newLine))));
        break;
    case 'add': //*** doc peut ne pas avoir de valeur ???
        $file=Pather('doc');
        $container=$_POST['container'];
        //echo $file;
        //echo 'ajax:enregistre la saisie dans le fichier '.$file;
        $content = '';
        if ($_POST['replaceContent'] != 'true')
            $content = file_get_contents($file);
        $content = $_POST['data'].PHP_EOL.$content;
        $content = indentations_control($content); // to avoid illegal indentations
        file_put_contents("$DATA/$file",$content); // store the modification on the disk
        $content = PKDP_tree($container,$file,true); // load the tree in the column
        echo $content; //div_for_status("$DATA/".$source);
        break;
    case 'readContent': // read file content (for display or edit) - giving path
        $file=Pather('file');
        //echo "<b>file=$file<b><br/>";
        if ($_POST['mask'] != 'no')
            $mask =  '<b>'.basename($file).'</b><br><hr><pre>%s</pre>'; // affichage simple avec le nom du fichier
        else
            $mask = '<textarea id="input">%s</textarea>'; // affichage du contenu pour saisie sans le nom du fichier (pourquoi?)
        echo sprintf($mask,file_get_contents("$DATA/$file"));
        break;
    case 'loadFolder':
        $source = str_replace('ø','/',$_POST['source']);
        $container = $_POST['container'];
        echo PKDP_folder($container,$source,true);
        break;
    case 'loadTree':
        $source = str_replace('ø','/',$_POST['source']);
        //$destination = str_replace('ø','/',$_POST['destination']); // n'est pas utilisé...
        $container = $_POST['container'];
        //$status = file_get_contents(PKDP_path_status($source,'',1));
        //unset($_SESSION['elements'][$destination]); // forget the previous cep
        echo PKDP_tree($container,$source,true); // load the tree in the column
        break;
    case 'empty':
        $file=Pather('path');
        $cep=$_POST['cep'];
        file_put_contents($file,'rien'); // empty the file
        $content = PKDP_tree($cep,$file,true);
        echo $content.div_for_status("$DATA/".$cep);;
        break;
    // case 'status': // store the status (and show it)
    //     file_put_contents(PKDP_path_status($_POST['cep']),$_POST['status']);
    //     echo $_POST['status']; // in the status-cep div
    //     break;
}
