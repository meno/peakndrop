<?php

class Atom
{
  public $nature;
  public function __construct($n){
    $this->nature = $n;
  }
}
class Text extends Atom
{
  public $description;
  public function __construct($n, $d){
    parent::__construct($n);
    $this->description = $d;
  }
  function html5(){
    return "$this->nature<br>$this->description";
  }
}


class Image extends Atom {
  public $url;
  public function __construct($n,$url){
    parent::__construct($n);
    $this->url = $url;
  }
  public function html5(){
    return "<figure><img src='$this->url' height='150' width='90'><figcaption>$this->nature</figcaption></figure>";
  } 
  public function latex(){
    return "\begin{figure}\includegraphics[width=\linewidth]{$this->url}\caption{$this->nature}\end{figure}";
  }
}

class Composite extends Text {
  function export($export){
    return preg_replace_callback('/@([0-9])*/ ',function ($matches) use ($export) { echo "*".$matches[1]."*";return truc($matches[1])->$export();},$this->description); 
  }
}

function truc($n){
  switch ($n){
  case 1:return new Text('mon texte','Mon joli texte avec plein de mots');
  case 2:return new Text('deuxième texte','Mon deuxième texte');
  case 3:return new Composite('Mon composite',"Il s'agit de deux textes: @1 et aussi @2 et c'est fini");
  }
}

echo truc(3)->export('html5');

?>
