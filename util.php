<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$path=getcwd();

if (!isset($_SESSION))
  session_start();
$DATA = "$path/users/".$_SESSION['user']; // directory to store folders (and trees)
$IMAGES = "$path/images";

require_once('menus_and_buttons.php'); // design of the page

$debug=false;
if ($debug) {
  echo "<pre>";
  print_r($_POST);
  echo "</pre>";
  }

//var_dump($_POST);

function view($table,$name=''){
  echo "<hr><b>$name</b><pre>";
  print_r($table);
  echo "</pre><hr>";
}
function string2array($content){ // get table with no empty lines at the end
  $table=explode(PHP_EOL, $content);
  while (sizeof($table)>0 and trim(end($table))=='') // remove line breaks at the end (unix) 
    array_pop($table);
  return $table;
}

function array2string($table){ // store table in file
  return implode(PHP_EOL, $table);
}

function file_status($file){
  return dirname($file).'/.'.basename($file).'.status';
}

function getOrCreateFile($filename)
{
    if (!file_exists($filename))
        file_put_contents($filename, '');
    return file_get_contents($filename);
}

function div_for_status($file){
  $name=basename($file);
  return "<div id='status-$name' class='hidden'>".getOrCreateFile(file_status($file)).'</div>';
}

// filter le text of a text-tree to hide the numbers at the beginning of the line (number of the ressource)
function filter_text($text){ // hide number of ressources "@1234 text" -> "<!--@1234-->text"
  return preg_replace('/^( *)@(\d+)([a-z]+) (.*)$/mu','\1<!-- @\2 --><img width=16 src="images/icons/32x32/\3.png"> \4',$text);
}

function load_tree($name,$path,$only_content=false) { // if $content=true; only content, not div (for ajax return)
  global $DATA;
  $file = $DATA.'/'.$path;
  $div='%s';
  if (file_exists($file))
    {
    if (!$only_content)
      $div="<div url='".str_replace('/','ø',$path)."' id='".str_replace('/','ø',$path)."' container='$name' class='root tree'>%s</div>";
    $text = file_get_contents($file);
    $text = filter_text($text);
    $a = indent2ulli(string2array($text),'&nbsp;<div class="title" menu="line">%s</div>');
    $result = sprintf($div,str_replace(['<li>'],["<li class='item list'>"],$a));
    $result .= div_for_status($file);
    return $result;
    }
  else return '';
}

function indentOf($s){ 
  return strlen($s) - strlen(ltrim($s));
}

function indentations_control($s) { // to avoid illegal indentations
  $i = 0;
  $s = explode("\n",$s);
  foreach($s as &$line){
      $indent = indentOf($line);
      if ($indent-$i > 1)
          $line = str_repeat(' ',$i+1).trim($line);
      $i = $indent;
  }
  return implode("\n",$s);
}

/*function PKDP_path($cep){
  global $DATA;
  return "$DATA/".$_SESSION['elements'][$cep];
}*/

/*function PKDP_path_status($cep){ // return real path of the status file
  //echo "path=$path ondisk=$ondisk<br>";
  $path = PKDP_path($cep);
  return dirname($path)."/.".basename($path).'.status';
} */ 

function indent2ulli($a,$format) { 
  if ($a == [])
    return '';
  $indent=-1;
  $r = '';
  foreach ($a as $line) {
       $i=indentOf($line);
       $line = sprintf($format,"<div class='line item wmenu' menu='line'>$line</div>");
       //$line = sprintf($format,$line); ancienne ligne avant gestion globale des menus
       if ($i == $indent)
         $r .= "\n<li>$line";
       else
          if ($i>$indent)
              $r .= "\n<ul>\n <li>$line"; // 1!
          else
              if ($i<$indent)
                  $r .= str_repeat("\n</ul>\n</li>",$indent-$i)."\n<li>$line";
      $indent = $i;
      }
  $r .= str_repeat("\n</li>\n</ul>",$indent+1);
  return $r;
}

function load_folder($name,$d, $first = 1) {
  global $DATA;
  $r = '';
  if (!$first)
    $r.= "<li class='item folder ondisk'>&nbsp;<div class='title wmenu' menu='folder'>".basename($d).'</div>';
  else {
    $r .= "<div id='".str_replace('/','ø',$d)."' container='$name' class='root tree ondisk'>"; // 1.if root, leave root - see ajax.php  2.avoid / in variables
    $d = "$DATA/$d";
    }
  $r .= '<ul>';
  foreach (scandir("$d") as $f) {
    if ($f != '.' and $f != '..') {
      if (is_dir("$d/$f"))
        $r.=load_folder($name,"$d/$f", 0);
      else
        if (basename($f)[0] != '.') // not an hidden file
          $r .= "<li class='item file ondisk'>&nbsp;<div class='title wmenu' menu='file'>$f</div></li>";
    }
  }
  $r .= '</ul>';
  if (!$first)
    $r .= '</li>';
  else
   {
    chdir('..');
    $r .= '</div>';
   }
  return $r;
}

function PKDP_packed($name,$url,$ondisk,$content,$content_only=false) {
  $mask = ($content_only) ? '%s' : "<div id='container-$name' class='treecontainer'>%s</div>";
  return sprintf($mask,"<div class='treelabel $ondisk'><b>$name</b><br/><div class='dir'>$url</div><br/>".PKDP_button_for_tree($name,$url,$ondisk)."</div>
        $content");
}
function PKDP_register($name,$url,$ondisk){
  //global $status;
  $_SESSION['elements'][$name]=$url; // register element
  if (!isset($_SESSION[$name]))
    $_SESSION[$name]=array();
  $_SESSION[$name][]=array($ondisk,$url);
  //$status_file = PKDP_path_status($name,$url,$ondisk);
  //$status[$name] = (file_exists($status_file)) ? file_get_contents($status_file) : ''; // register status_
}
function PKDP_button($name,$url){ // display the name of a file as a simple location to drop into
  //global $status; // to store objects and pass to javascript at the end of this script
  PKDP_register($name,$url,true);
  return "<div id='$name' class='root ondisk file tree nostyle'>
    <ul>
    <li url='$url' class='item button file ondisk nostyle'><div class='title button'>$name</div></li>
    </ul>
  </div>";
  /*<ul>
      <li class='item ondisk button file'><div class='title'>$name</div></li>
  </ul>
</div>";*/
}
function PKDP_tree($name,$url,$content_only=false){ // display the content of of a indented file as a tree
  global $status,$DATA;
  PKDP_register($name,$url,false);
  return PKDP_packed($name,$url,0,load_tree($name,$url),$content_only);
}

function PKDP_folder($name,$url){ // display the content of a folder as a filesystem
  global $status;
  PKDP_register($name,$url,true);
  return PKDP_packed($name,$url,1,load_folder($name,$url,1));
}

function PKDP_buttons($Array,$name='',$url='',$nactions=''){
  global $dim_icons;
  $result = '';
  foreach ($Array as $key => $value)
    $result .= "<img class='img_button' onclick=\"".$value[1]."('$url','$name');\" alt='".$value[0]."' src='images/actions$nactions/$key.png' style='width:$dim_icons;height:$dim_icons' title='".$value[0]."'/>";
  return $result;
}

function PKDP_button_for_tree($name,$url,$ondisk) { // buttons to deal with an entire tree, first in container
  global $PKDP_tree_buttons,$PKDP_folder_buttons,$PKDP_actions2;
  if ($ondisk)
    return PKDP_buttons($PKDP_folder_buttons,$name,$url);
  else
    return PKDP_buttons($PKDP_tree_buttons,$name,$url).'<br>'.PKDP_buttons($PKDP_actions2,$name,$url,2);
}

