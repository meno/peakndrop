<!-- for information return from ajax -->
<div id='info'>
</div>

<!-- dialog for input -->
<dialog id="dialog" class="dialog" onsubmit="event.preventDefault();sendInput();">  
    <form>
        <div id="textarea"><textarea id="input"></textarea></div>
        <input type="submit" value="OK">
    </form>  
</dialog>  
<dialog id="view" onclick="this.close();" >
</dialog>

<?php
echo $PKDP_menus;
?>

<style>
:root {
    --font-family: "Trebuchet MS", sans-serif;
    --background-color: white;
    --text-color: black;
    --tooltip-background: black;
    --tooltip-text-color: #fff;
    --tooltip-arrow-color: transparent;
    --highlight-background: rgba(0, 204, 255, 0.2);
    --dialog-background: lightblue;
    --container-gap: 3px;
    --container-min-height: 200vh;
    --tree-background: darkslategrey;
    --treelabel-background: #bbb;
    --tree-radius: 12px;
    --treelabel-color: maroon;
    --root-background:darkgray;
    --ondisk-background: black;
    --folder-label-color: black;
    --list-label-color: ivory;
    --color0: ivory;
    --color1: yellow;
    --color2: orange;
    --color3: tomato;
}

body {
    font-family: var(--font-family);
    background: var(--background-color);
    color: var(--text-color);
}

.treelabel{
    overflow:hidden;
}

.tree { 
    margin:10px;
    overflow:auto;
}

/* dir */
.dir {
    font-size: 70%;
    margin-top: -6px;
    margin-bottom: -22px;
}
/* end of dir */

.container {
    display: flex;
    gap: var(--container-gap);
    height:80vh;
}

.tree {
    text-align: left;
    padding:2px;
    margin:0px;
    height:67vh;
    border-radius: 0 0 var(--tree-radius) var(--tree-radius);
    overflow-y:visible;
    overflow-x:hidden;
}

.treecontainer {
    flex-basis: 50%;
    background: #ddd;
    /*min-height: var(--container-min-height);*/
    border-radius: var(--tree-radius);
    background: var(--tree-background);
    padding:4px;
    height:90vh;
}


.treelabel {
    display: block;
    text-align: center;
    font-size: 18px;
    min-height: 50px;
    color: var(--treelabel-color);
    background: var(--treelabel-background);
    padding-bottom: 2px;
    border-radius: var(--tree-radius) var(--tree-radius) 0 0;
    margin-bottom: 0px;
}
    

.item {
    margin: 0 3px;
    padding-left: 5px;
    border-radius: 0px;
    cursor: pointer;
    bottom: 2px;
}

.root{
    background-color:var(--root-background);
}

.ondisk{
    background-color:var(--ondisk-background);
}

/* Ajout d'une règle pour s'assurer que les dropzones occupent toute la largeur et hauteur */
.dropzone {
    flex-grow: 1; /* Permet à la zone de prendre tout l'espace disponible */
    /*height: 100%;*/ /* S'assure que les zones occupent toute la hauteur */
}

.dropzone[drop-active=true] {
    background-color: var(--highlight-background);
    min-height: 50px; /* Peut être ajusté selon les besoins */
}

.inter {
    background: none;
    height: 8px;
    width: 100%;
    padding-right: 5px;
    margin: -1px -5px -5px 0px;
}

.invisible {
    display: none;
}

ul {
    padding-left: 0;
    list-style: none;
    margin-top: 3px;
}

ul ul {
    padding-left: 1em;
}

li .inter[drop-active=true] {
    background: var(--highlight-background);
}

.label_for_hidding_button, .labeld_for_hidding_button {
    float: left;
    color: var(--text-color);
}

.hidding_button:checked ~ ul {
    display: none;
}

label {
    position: relative;
    z-index: 1;
    width: 1em;
    height: 1em;
    border-radius: 0.3em;
    line-height: <?= $lineHeight;?>em;
}

.folder label {
    margin: 4px 2px -2px -4px;
    color: var(--folder-label-color);
}

.list label {
    margin: 4px 2px -2px -2px;
    color: var(--list-label-color);
}

/* for lists */
.list label.label_for_hidding_button:before {
    content: '\229F'; /* ⊟ */
}

.list :checked ~ label.label_for_hidding_button:before { 
    content: '\229E'; /* ⊞ */
}

.folder label.label_for_hidding_button:before {
    content: '\1F4C2'; /* 🗁 */
}

.folder :checked ~ label.label_for_hidding_button:before { 
    content: '\1F4C1'; /* 🗀 */
}

/* original idea http://www.bootply.com/phf8mnMtpe */

.tree li {
    list-style-type: none;
    position: relative;
    white-space: nowrap;
}

.tree li::before, .tree li::after {
    content: '';
    left: -0.75em;
    position: absolute;
    right: auto;
}

.tree li.invisible_tree_link::before, .tree li.invisible_tree_link::after {
    content: none;
}

.tree li::before {
    border-left: 1px solid gray;
    height: 110%;
    margin-left: -2px;
    margin-top: 0px;
    top: <?= $top;?>em;
}

.tree li::after {
    border-top: 1px solid gray;
    top: <?= $dash; ?>em;
    margin-left: -2px;
}

.tree .list li::after {
    width: 1.3em;
}

.tree .folder li::after {
    width: 1.0em;
}

.tree > ul > li::before, .tree > ul > li::after {
    border: 0;
}

.tree li:last-child::before {
    height: <?= $last;?>em;
    padding: 1px;
}

li.folder .title {
    font-weight: bold;
}

li.file .title {
    font-weight: normal;
}

/* ----------------- specific style for file system ----------- */

.title {
    margin-left: -2px;
    line-height: <?= $lineHeight;?>em;
    display: inline-block;
}

.nostyle {
    all: unset;
}

.button {
    border-radius: 12px;
    padding: 5px;
    text-align: center;
    background: darkgrey;
    color: var(--text-color);
}

/* ------------------ colors --------------------- */
.color0 {
    color: var(--color0);
}

.color0.button {
    all: unset;
}

.color1 {
    color: var(--color1);
}

.color2 {
    color: var(--color2);
}

.color3 {
    color: var(--color3);
    font-weight: bold;
}

/* --------------- for buttons -------------------- */
.img_button {
    display: inline;
    width: 24px;
    height: 24px;
    margin: 0;
    margin-right: 3px;
}

/* --------------- for dialogs -------------------- */
.hidden {
    display: none;
}

.action {
    padding: 2px;
    background-color: lightblue;
    margin: 0px;
    border: 1px black solid;
}

.dialog {
    border: 0px;
}

.menu {
    border: 0px solid black;
    padding: 0px;
    margin: 0px;
    background: red;
    z-index: 10000;
}

#dialog {
    z-index: 10000;
    background: var(--dialog-background);
    border: 0px;
}

#input {
    border: 1px;
    width: 600px;
    height: 100px;
    z-index: 10000;
    resize: both;
}

#view {
    z-index: 10000;
    background: var(--dialog-background);
    border: 1px;
    height: 400px;
    width: 400px;
}

#visu {
    margin: 0;
    padding: 0;
}
</style>
