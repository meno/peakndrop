# Peakndrop

## Principe

Peakndrop est un système permettant de gérer (avec les mêmes routines) des déplacements de fichiers ou de répertoires, et de lignes indentées dans des fichiers bruts.  Le client mobile disposera des mêmes fonctionnalités.

Il se présente comme un ensemble de zones (raisonnablement trois ou quatre). Ces zones contiennent des arborescences (contenus indentés de fichiers bruts, ou répertoires). Des actions sont possibles entre ces zones. Considérons les deux types de zones comme étant au point de départ de l'action.

### Actions entre zones

|                |                           |                         |            Zone de départ            |                                      |                                      |                                      |      |      |
| -------------- | ------------------------- | :---------------------: | :----------------------------------: | :----------------------------------: | :----------------------------------: | :----------------------------------: | ---- | ---- |
|                | **Répertoire**            |   **MAJ Répertoire**    |             **Fichier**              |           **MAJ Fichier**            |              **Arbre**               |            **MAJ Arbre**             |      |      |
| **Répertoire** | Déplacement               |         *Copie*         |             Déplacement              |               *Copie*                |                                      |                                      |      |      |
| **Fichier**    |                           |                         | *Ajout au début (avec confirmation)* | *Ajout à la fin (avec confirmation)* | Ajout au début (*avec confirmation*) | *Ajout à la fin (avec confirmation)* |      |      |
| **Arbre**      | *Répertoire dans la zone* | *Permutation des zones* |  Ouverture du fichier dans la zone   |       *Permutation des zones*        |             Déplacement              |               *Copie*                |      |      |

### Action du clic

| Zone           | clic                                                   | MAJ clic                             |
| -------------- | ------------------------------------------------------ | ------------------------------------ |
| **Répertoire** | Pliage/dépliage                                        |                                      |
| **Fichier**    | ouverture du contenu dans une fenêtre modale           | Ouverture du contenu dans cette zone |
| **Arbre**      | *Édition du fichier avec positionnement à cette ligne* | *Suppression*                        |



### Boutons d'action sur zone

Des boutons permettent d'agir sur toute une zone

### Menus déroulant au clic droit

Exemple: renommer



## Documentation développeur



Réquisit

### Configuration 

Déplier tout dans un répertoire accessible par votre serveur web.

#### Configuration sur place

#### Configuration en ligne

Protéger le fichier de configuration pour pouvoir l'éditer en ligne



## Documentation d'installation

## Documentation utilisateur

## Remerciements

https://stackabuse.com/drag-and-drop-in-vanilla-javascript/ 



-----

-> traduire en anglais

!Principe

!!sous-titre

gestion d'arbre de fichier / gestion de fichiers contenant un arbre (en utilisant la même technique)

chaque arbre X est dans un conteneur de id=container-X
qui contient
- des boutons d'action sur l'arbre
- l'arbre lui-même

Les données sont dans un répertoire data dans lequel le système doit pouvoir écrire... pour écrire

peak.php et util.php contiennent les fonctions principales de construction
js/peak.js et css/peak.css.php répond aux actions de l'utilisateurs
    ajax.php est appellée par js/peak.js
js/dialog.js gère les affichages/saisie dans une fenètre modale avec envoie à ajax.php par la function sendInput

Le système est capable de se souvenir de l'atat des arbres (fermés ou non ici ou là) par un système de statuts (fichiers .status dans le répertoire status)

l'arbre est chargé en php (ou en php en ajax)
il est stylé derrière en javascript pour la gestion de ses événements

actions possibles
1. boutons
2. bouton droit sur un item
3. drag'n drop - avec ou dans la touche MAJ activée
    - fichier sur répertoire - déplacement ou copie
    - qdq



 



