<?php

class Atom
{
  public $nature;
  public function __construct($n){
    $this->nature = $n;
  }
}
class Text extends Atom
{
  public $description;
  public function __construct($n, $d){
    parent::__construct($n);
    $this->description = $d;
  }
}
$e = new Text(10,5);
var_dump($e);
?>
